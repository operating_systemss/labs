#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


int main()
{
    pid_t pid1,pid2;

    printf("Pid: %d Parent PID: %d\n",getpid(),getppid());

    pid1=fork();

    if(pid1 == 0)
    {
         printf("Pid: %d Parent PID: %d\n",getpid(),getppid());
        if(fork() == 0)
        {
             printf("Pid: %d Parent PID: %d\n",getpid(),getppid());
            sleep(60);
        }
        else
        {
            wait(NULL);
        }
    }
    else
    {
        pid2=fork();
        if(pid2 == 0)
        {
            printf("Pid: %d Parent PID: %d\n",getpid(),getppid());

            if(fork() == 0)
            {
                 printf("Pid: %d Parent PID: %d\n",getpid(),getppid());
                sleep(60);
            }
            else
            {
                wait(NULL);
            }
        }
        else
        {
            waitpid(pid1,NULL,0);
            waitpid(pid2,NULL,0);
        }
    }


    return 0;
}