#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>



int main()
{
    int i,n=10;
    pid_t pidStart=getpid();
    pid_t currPid;
    int nr=1;
    int base=0;

    for(i=0;i<n;i++)
    {
        int status;
        currPid=fork();

        if(currPid)
        {
            nr=1;
            wait(&status);
            nr+=WEXITSTATUS(status);
            if(nr>=256)
            {
                base++;
                nr=base;
            }
        }
    }
    if(getpid() == pidStart)
    {
        if(base == 0)
        {
            printf("%d\n",nr);
        }
        else
        printf("%d\n",nr*256);
    }
    exit(nr);

    return 0;
}