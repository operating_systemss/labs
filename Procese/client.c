#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <stdio.h>

int main()
{int intreg1,intreg2;
char operatie;

   for(;;)
   {
       printf(">");
       scanf("%d %d %c",&intreg1,&intreg2,&operatie);

       char arg1[4];
       char arg2[4];

       snprintf(arg1,sizeof(arg1),"%d",intreg1);
       snprintf(arg2,sizeof(arg2),"%d",intreg2);

       if(fork() == 0)
       {
           execlp("./server","server",arg1,arg2,&operatie,(char*)0);
           printf(("fail\n"));
       }
       else
       {
           int status;
           wait(&status);
           printf("%d\n",WEXITSTATUS(status));
       }
   }

    return 0;
}