#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>


int main(int argc,char** argv)
{
    int fd;
    int save_std_out;

    save_std_out=dup(1);


    fd=open("out.txt",O_RDWR);
    dup2(fd,1);


    if(fork() == 0)
    {
    execlp("grep","grep","abcd",argv[1],NULL);
    }
    else
    {
        wait(NULL);
        lseek(fd,0,SEEK_SET);
        dup2(save_std_out,1);
        dup2(fd,0);
        execlp("wc","wc","-l",NULL);
    }

    return 0;
}