#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/wait.h>
#define N 3


typedef struct child{
    pid_t pid;
    int low;
    int high;
}child;

bool nrPrim(int x)
{
    for(int i=2;i<=x/2;i++)
    if(x%i==0)
    return false;

    return true;
}

int main(int argc,char **argv)
{
    if(argc!=3)
    {
        printf("Usage: %s <lower bound upper bound>\n",argv[0]);
        return -1;
    }
    int low,high;
    int nr=1;
    int memorie;
    int* v=NULL;


    memorie=shm_open("/memorie1",O_CREAT|O_RDWR,0600);

    ftruncate(memorie,nr*sizeof(int));

    v=(int*)mmap(NULL,sizeof(int),PROT_READ|PROT_WRITE,MAP_SHARED,memorie,0);


    *v=nr-1;

    sscanf(argv[1],"%d",&low);
    sscanf(argv[2],"%d",&high);

    if(low>=high)
    {
        printf("Interval invalid\n");
        return -1;
    }

    child *a = (child*)malloc(N*sizeof(child));

    int parentPid=getpid();

    int s=high-low;
    int ad=s/N;

   a[0].low=low;
   a[0].high=a[0].low+ad;

   for(int i=1;i<N-1;i++)
   {
       a[i].low=a[i-1].high+1;
       a[i].high=a[i].low+ad;
   }
   a[N-1].low=a[N-2].high+1;
   a[N-1].high=high;


   for(int i=0;i<N;i++)
   {
       a[i].pid=fork();
       if(a[i].pid == 0)
       {
           int memorie;
           int* v=NULL;

           memorie=shm_open("/memorie1",O_RDWR,0);
           v=(int*)mmap(NULL,nr*sizeof(int),PROT_WRITE|PROT_READ,MAP_SHARED,memorie,0);
           int j=0;
           int low=a[i].low;
           int high=a[i].high;
           for(j=low;j<=high;j++)
           {
               if(nrPrim(j)==true)
               {
                   ftruncate(memorie,(v[0]+1)*sizeof(int));
                   v[0]=v[0]+1;
                   v[v[0]]=j;
               }
           }
           
           exit(0);
       }
    
   }

   if(getpid() == parentPid)
   {
       for(int i=0;i<N;i++)
       waitpid(a[i].pid,NULL,0);
       
      for(int i=0;i<v[0]+1;i++)
      printf("%d ",v[i]);

      munmap(v,(v[0]+1)*sizeof(int));

   }

    return 0;
}