#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>


int main(int argc,char** argv)
{
    if(argc!=2)
    {
        printf("Usage: %s <filename>\n",argv[0]);
        return -1;
    }

    int fd;
    off_t size;
    char* data=NULL;

    fd=open(argv[1],O_RDWR);
    if(fd == -1)
    {
        perror("Fisierul nu a putut fi deschis");
        return -1;
    }
    size=lseek(fd,0,SEEK_END);
    lseek(fd,0,SEEK_SET);
    data=(char*)mmap(NULL,size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);

    if(data == (void*)-1)
    {
        perror("Nu s-a putut face maparea");
        return -1;
    }

    for(int i=0;i<size;i++)
    {
        if(data[i]=='A' || data[i]=='E' || data[i] == 'O' || data[i]=='U' || data[i]=='I'
        || data[i]=='a' || data[i]=='e' || data[i] == 'o' || data[i]=='u' || data[i]=='i')
        {
            data[i]=' ';
        }
    }

    for(int i=0;i<size;i++)
    {
        if(data[i]==' ')
        {
            int aux=i+1;
            while(data[aux]==' ' && aux<size-1)
            {
                aux++;
            }

            if(aux==size)
            break;
            int aux2=data[aux];
            data[aux]=data[i];
            data[i]=aux2;
            
        }
    }

    munmap(data,size);
    close(fd);
    data=NULL;
    

    return 0;
}