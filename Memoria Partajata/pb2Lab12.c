#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>

int main()
{
    int memPartajata;
    int *var=NULL;
    sem_t *sem1,*sem2;

    sem_unlink("semafor1");
    sem_unlink("semafor2");
    
    unlink("/memorie");
    memPartajata=shm_open("/memorie",O_CREAT | O_RDWR, 0600);
    sem1=sem_open("semafor1",O_CREAT,0644,1);
    sem2=sem_open("semafor2",O_CREAT,0644,1);

    if(memPartajata<0)
    {
        perror("Nu s-a putut partaja memoria");
        return -1;
    }
    ftruncate(memPartajata,sizeof(var));


    var=(int*)mmap(NULL,sizeof(int),PROT_WRITE|PROT_READ,MAP_SHARED,memPartajata,0);

    *var=0;

    if(fork() != 0)
    {sem_wait(sem1);
    sem_wait(sem2);

        while(1)
        {
        sem_wait(sem1);
        *var=*var+1;
        printf("Variabila a fost incrementata de parinte: %d\n",*var);
        sem_post(sem2);
        sleep(1);
        }
    }
    else
    {
        while(1)
        {
            sem_post(sem1);
            sleep(1);
            sem_wait(sem2);
            *var=*var+1;
            printf("Variabila a fost incrementata de copil: %d\n",*var);
    
        }

    }

    sem_close(sem1);
    sem_close(sem2);
    munmap(var,sizeof(int));
    var=NULL;




    return 0;
}
