#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc,char** argv)
{
    if(argc!=2)
    {
        printf("Usage: %s <filename>\n",argv[0]);
        return -1;
    }
    

    int fd;
    off_t size;
    char* data=NULL;

    fd=open(argv[1],O_RDWR);
    if(fd == -1)
    {
        perror("Fisierul nu a putut fi deschis");
        return -1;
    }
    size=lseek(fd,0,SEEK_END);

    lseek(fd,0,SEEK_SET);
    data=(char*)mmap(NULL,size,PROT_READ | PROT_WRITE,MAP_SHARED,fd,0);


    if(data == (void*)-1)
    {
        perror("Nu s-a putut face maparea");
        return -1;
    }

    off_t i=0;
    off_t j=size-2;

    while(i<j)
    {
        char aux=data[i];
        data[i]=data[j];
        data[j]=aux;
        i++;
        j--;
    }

    munmap(data,size);
    close(fd);

   

    return 0;
}
