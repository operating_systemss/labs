#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int v[10]={1,2,3,4,5,6,7,8,9,10};
int n = 10;
int nrDiv[10];
int i;
pthread_mutex_t mutex;

void* thread(void* arg)
{
    for(;;)
    {
        pthread_mutex_lock(&mutex);
        if(i>=n)
        {
            pthread_mutex_unlock(&mutex);
            break;
        }
        else
        {
            int nr=0;
            for(int j=1;j<=v[i];j++)
            {
                if(v[i]%j==0)
                nr++;
            }
            nrDiv[i]=nr;
            i++;
            printf("Thread ul %d a numarat divizorii numarului %d\n",(int)(long)arg,v[i]);
            pthread_mutex_unlock(&mutex);
            usleep(100);
        }
    }
    return NULL;
}
int main(int argc,char** argv)
{
    if(argc!=2)
    {
        printf("format: %s <nrThreads>",argv[0]);
        return 1;
    }

    int nrThreads=atoi(argv[1]);

    pthread_t *threads = malloc(nrThreads*sizeof(pthread_t));

    pthread_mutex_init(&mutex,NULL);

    for(int i=0;i<nrThreads;i++)
    pthread_create(&threads[i],NULL,thread,(void*)(long)(i+1));

    for(int i=0;i<nrThreads;i++)
    pthread_join(threads[i],NULL);


    for(int i=0;i<10;i++)
    printf("%d ",nrDiv[i]);




    return 0;
}