#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

pthread_mutex_t mutex;
int fd = -1;
int open_once()
{
    if(fd == -1)
    {
        printf("Fisier deschis\n");
        fd = open("file",O_CREAT|O_RDONLY);
    }
    return fd;
}

void* thread(void* arg)
{
    pthread_mutex_lock(&mutex);
    int fd=open_once();
    pthread_mutex_unlock(&mutex);
    char c;
    read(fd,&c,1);
    printf("%c\n",c);

    return NULL;
}

int main()
{
    pthread_t td[3];

    for(int i=0;i<3;i++)
    pthread_create(&td[i],NULL,thread,NULL);

    for(int i=0;i<3;i++)
    pthread_join(td[i],NULL);


    return 0;
}