#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#define numarMaximMasiniPePod 5
int numarMasiniPePod;
int directieDeplasarePePod=-1;//0 spre dreapta pe pod , 1 spre stanga
pthread_mutex_t lock;
pthread_cond_t cond;
pthread_cond_t cond2;
pthread_mutex_t lock2;
pthread_mutex_t lock3;
long int *IDS;
int numberOfIds;
int n;
long int pop()
{
    long int x=0;
    if(numberOfIds == 0)
    return -1;
    else
    {
        x=IDS[0];
        for(int i=0;i<numberOfIds-1;i++)
        IDS[i]=IDS[i+1];
        numberOfIds--;
    }
    return x;
}

void push(long int x)
{
    if(numberOfIds == n)
    return ;
    else
    {
       IDS[numberOfIds]=x;
       numberOfIds++;
    }
}
long int firstElem()
{
    if(numberOfIds == 0)
    return -1;
    else
    return IDS[0];
}

void exit_bridge(int dir)
{
    pthread_mutex_lock(&lock2);
    printf("Masina cu ID-ul %ld iese de pe pod\n",pthread_self());
    numarMasiniPePod--;
    if(numarMasiniPePod == 0)
    directieDeplasarePePod=-1;
    pthread_cond_broadcast(&cond2);
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&lock2);

}
void cross_bridge(int dir)
{
   
   
    pthread_mutex_lock(&lock3);
    while(pthread_self()!=firstElem()){
     pthread_cond_wait(&cond2,&lock3);
    }

    if(dir==0)
 printf("Masina cu ID-ul %ld traverseaza podul,va dura 5 secunde,directia de deplasare e in dreapta\n",pthread_self());
 else
 printf("Masina cu ID-ul %ld traverseaza podul,va dura 5 secunde,directia de deplasare e in stanga\n",pthread_self());

 pop();

 pthread_cond_broadcast(&cond2);

 sleep(5);
 pthread_mutex_unlock(&lock3);

}
void enter_bridge(int dir)
{
    pthread_mutex_lock(&lock);
    while((directieDeplasarePePod != -1 && directieDeplasarePePod != dir) || (numarMasiniPePod == numarMaximMasiniPePod)){
        printf("Masina cu ID-ul %ld trebuie sa astepte\n",pthread_self());
    pthread_cond_wait(&cond,&lock);
    }
    if(directieDeplasarePePod == -1)
    {
    printf("Masina cu ID-ul %ld a intrat pe podul liber\n",pthread_self());
    push(pthread_self());
    directieDeplasarePePod = dir;
    numarMasiniPePod++;
    }
    else{
        printf("Masina cu ID-ul %ld a intrat pe podul deja traversat de alte masini\n",pthread_self());
        push(pthread_self());
    numarMasiniPePod++;
    }
    pthread_mutex_unlock(&lock);
}

 void *car_thread(void *direction)
 {
 int dir = (int)(long)direction;
 enter_bridge(dir);
 cross_bridge(dir);
 exit_bridge(dir);
 return NULL;

 }

int main()
{
    printf("Introduceti numarul de masini:\n");
    scanf("%d",&n);
    pthread_t *tids=(pthread_t*)malloc(n*sizeof(pthread_t));
    IDS=(long int*)calloc(n,sizeof(long int));
    pthread_mutex_init(&lock,NULL);
    pthread_cond_init(&cond,NULL);
    pthread_cond_init(&cond2,NULL);
    pthread_mutex_init(&lock2,NULL);
    pthread_mutex_init(&lock3,NULL);

    srand(time(0));

    for(int i=0;i<n;i++)
    pthread_create(&tids[i],NULL,car_thread,(void*)(long)(rand()%2));

    for(int i=0;i<n;i++)
    pthread_join(tids[i],NULL);

    pthread_mutex_destroy(&lock);
    pthread_mutex_destroy(&lock2);
    pthread_mutex_destroy(&lock3);
    pthread_cond_destroy(&cond);
    pthread_cond_destroy(&cond2);
    return 0;
}