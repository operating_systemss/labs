#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

void delete(char* dirPath)
{
    DIR* dir = NULL;
    struct dirent *entry = NULL;
    struct stat statbuf;
    char fullPath[512];

    dir=opendir(dirPath);

    if(dir == NULL)
    {
        return;
    }

    while((entry=readdir(dir)) != NULL)
    {
        if(strcmp(".",entry->d_name) != 0 && strcmp("..",entry->d_name) != 0)
        {
            snprintf(fullPath,512,"%s/%s",dirPath,entry->d_name);
            if(lstat(fullPath,&statbuf) == 0)
            {
                if(S_ISREG(statbuf.st_mode) || S_ISLNK(statbuf.st_mode))
                {
                    unlink(fullPath);
                }
                else
                {
                    if(S_ISDIR(statbuf.st_mode))
                    {
                        delete(fullPath);
                        rmdir(fullPath);
                    }
                }
            }
        }
    }
}

int main(int argc,char** argv)
{
    delete(argv[1]);
    rmdir(argv[1]);



    return 0;
}