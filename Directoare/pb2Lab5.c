#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
int contor;

void search(char* DirPath,char* fileName,char* String)
{
    DIR* dir = NULL;
    struct dirent *entry = NULL;
    struct stat statbuf;
    char fullPath[512];

    dir=opendir(DirPath);

    if(dir == NULL)
    {
        return;
    }

    while((entry = readdir(dir))!= NULL)
    {
        if(strcmp(entry->d_name,".") != 0 && strcmp(entry->d_name,"..") != 0)
        {
            snprintf(fullPath,512,"%s/%s",DirPath,entry->d_name);
            if(lstat(fullPath,&statbuf) == 0)
            {
                printf("%s\n",fullPath);
                if(S_ISREG(statbuf.st_mode))
                {
                    if(strcmp(entry->d_name,fileName)==0)
                    {
                       
                        int fd = open(fullPath,O_RDONLY);
                        int size=lseek(fd,0,SEEK_END);
                        lseek(fd,0,SEEK_SET);


                        char * data = malloc((size+1)*sizeof(char));
                        read(fd,data,size*sizeof(char));
                        data[size]='\0';


                        char *p = strstr(data,String);

                        if(p!=NULL)
                        {
                            char name[9];
                            contor++;
                            sprintf(name,"sym.%d",contor);
                            char symPath[512];
                            strcpy(symPath,DirPath);
                            strcat(symPath,"/");
                            strcat(symPath,name);
                            symlink(fullPath,symPath);

                        }

                    }
                }
                    else
                    {
                        if(S_ISDIR(statbuf.st_mode))
                        {
                            search(fullPath,fileName,String);
                        }
                    }
            
            }
        }
    }

}


int main(int argc,char** argv)
{
    if(argc != 4)
    {
        printf("format: %s <FolderPath FileName String>",argv[0]);
        return 1;
    }

    search(argv[1],argv[2],argv[3]);





    return 0;
}