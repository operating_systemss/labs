#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <stdbool.h>


int v[1000]; //tine i-node urile
int contor;

bool isHere(int inode)
{
    for(int i=0;i<contor;i++)
    {
        if(v[i] == inode)
        return true;
    }
    
    return false;
}

int dirSize(const char* dirPath)
{
    DIR* dir = NULL;
    struct dirent *entry = NULL;
    struct stat statbuf;
    char fullPath[512];
    int sum=0;

    dir =  opendir(dirPath);

    if(dir == NULL)
    {
        printf("Nu putem deschide folderul\n");
        return 0;
    }

    while((entry = readdir(dir)) != NULL)
    {
        if(strcmp(entry->d_name,".") != 0 && strcmp(entry->d_name,"..") != 0)
        {
            snprintf(fullPath,512,"%s/%s",dirPath,entry->d_name);
            if(lstat(fullPath,&statbuf) == 0)
            {
                printf("%s\n",fullPath);
                if(S_ISREG(statbuf.st_mode))
                {
                    if(isHere(statbuf.st_ino) == false)
                    {sum+= statbuf.st_size;
                    v[contor] = statbuf.st_ino;
                    contor++;
                    }
                    
                }
                else
                {
                if(S_ISDIR(statbuf.st_mode))
                {
                   sum+= dirSize(fullPath);
                }
                }
            }

        }
    }
    return sum;
}



int main(int argc,char** argv)
{
    printf("%d\n",dirSize(argv[1]));


    return 0;
}