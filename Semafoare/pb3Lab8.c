#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define N 10
int contor=1;
sem_t sem;

void* thread(void* arg)
{
    for(;;)
    {
        srand(time(0));
        sem_wait(&sem);
        if((int)(long)arg == contor){
        printf("%d\n",(int)(long)arg);
        contor++;
        if(contor == N+1)
        {
            contor=1;
        }
        sleep(rand()%5);
        }
        sem_post(&sem);

    }
}
int main()
{
    pthread_t td[N];

    sem_init(&sem,0,1);

    for(int i=0;i<N;i++)
    pthread_create(&td[i],NULL,thread,(void*)(long)(i+1));

    for(int i=0;i<N;i++)
    pthread_join(td[i],NULL);

    return 0;
}