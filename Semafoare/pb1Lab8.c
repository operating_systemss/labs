#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define M 1000
#define N 4
long count;
sem_t *sem = NULL;

void* thread_function(void *unused)
{
    int i;
    long aux;

    for(i=0;i<M;i++)
    {
        sem_wait(sem);
        aux=count;
        aux++;
        usleep(random() % 10);
        count =  aux;
        sem_post(sem);
    }

    return NULL;
}


int main()
{
    pthread_t tids[N];
    sem=sem_open("semafor",O_CREAT,0644,1);

    for(int i=0;i<N;i++)
    pthread_create(&tids[i],NULL,thread_function,NULL);

    for(int i=0;i<N;i++)
    pthread_join(tids[i],NULL);

    printf("%ld\n",count);


    return 0;
}