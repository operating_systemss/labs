#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define N 4
int numberCl,numberNa;
sem_t semAnon;

typedef struct thread_Struct{
    int type; // 0 - Cl 1 - Na
    pthread_t t;
    int id;
}thread_Struct;

thread_Struct Na[N];
thread_Struct Cl[N];

void shift(thread_Struct v[])
{
    for(int i=0;i<N;i++)
    v[i]=v[i+1];

}
void* thread_function(void* arg)
{
    thread_Struct *t = (thread_Struct*)arg;

    if(t->type==0)
    {sem_wait(&semAnon);
    numberCl++;
    if(numberNa)
    {printf("S-a format molecula de sare din threadul cu id-ul %d si thread-ul cu id-ul %d\n",t->id,Na[0].id);
    shift(Na);
    pthread_cancel(Na[numberNa-1].t);
    numberCl--;
    numberNa--;
    sem_post(&semAnon);
    pthread_exit(NULL);
    }
    else
    {
        Cl[numberCl-1].t=pthread_self();
        Cl[numberCl-1].id=t->id;
        Cl[numberCl-1].type=t->type;
    }
    sem_post(&semAnon);
    }
    else
    {
        sem_wait(&semAnon);
        numberNa++;
        if(numberCl)
        {
            printf("S-a format molecula de sare din threadul cu id-ul %d si thread-ul cu id-ul %d\n",t->id,Cl[0].id);
            shift(Cl);
            pthread_cancel(Cl[numberCl-1].t);
            numberCl--;
            numberNa--;
            sem_post(&semAnon);
            pthread_exit(NULL);
        }
        else
        {
            Na[numberNa-1].t=pthread_self();
            Na[numberNa-1].id=t->id;
            Na[numberNa-1].type=t->type;
        }
        sem_post(&semAnon);
    }





    return NULL;
}


int main()
{
    thread_Struct tids[N];
    sem_init(&semAnon,0,1);
    srand(time(0));
    for(int i=0;i<N;i++)
    {tids[i].type=rand()%2;
    tids[i].id=i+1;
    }

    for(int i=0;i<N;i++)
    pthread_create(&tids[i].t,NULL,thread_function,&tids[i]);

    for(int i=0;i<N;i++)
    pthread_join(tids[i].t,NULL);

    printf("\n%d %d\n",numberCl,numberNa);


    return 0;
}