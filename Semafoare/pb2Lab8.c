#include <fcntl.h>
#include <sys/stat.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#define NRTHREADS 16
int nrThreads;
sem_t sem;
void *limited_area(void *unused)
 {
 sem_wait(&sem);
 nrThreads++;
 printf("The number of threads in the limited area is: %d\n", nrThreads);
 nrThreads--;
 sem_post(&sem);
 return NULL;
 }

int main(int argc,char** argv)
{
    int n=atoi(argv[1]);

    sem_init(&sem,0,n);

    pthread_t td[NRTHREADS];


    for(int i=0;i<NRTHREADS;i++)
    pthread_create(&td[i],NULL,limited_area,NULL);


    for(int i=0;i<NRTHREADS;i++)
    pthread_join(td[i],NULL);

    sem_destroy(&sem);


    return 0;
}