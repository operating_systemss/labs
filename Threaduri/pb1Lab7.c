#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#define NR_THREADS 3

typedef struct stru{
    int from;
    int to;
    char* data;
}stru;

void* thread(void* arg)
{
    int nr=0;
    stru *param=(stru*)arg;

    for(int i=param->from;i<=param->to;i++)
    {
        if(param->data[i] == '1')
        nr++;
    }

    return (void*)(long)nr;
}

int main()
{
    int n=14;
    char *integer = malloc(sizeof(int));
    char *data = malloc(n*sizeof(int));
    int nr=0;
    void* nr1;

    for(int i=1;i<=n;i++)
    {
        snprintf(integer,sizeof(int),"%d",i);
        strcat(data,integer);
    }

   long nrCifre = strlen(data);

   stru v[NR_THREADS];

   v[0].from = 0;
   v[0].to = nrCifre / NR_THREADS - 1;
   v[0].data = malloc(n*sizeof(int));
   strcpy(v[0].data,data);

   for(int i=1;i<NR_THREADS;i++)
   {
       v[i].from = v[i-1].to + 1;
       v[i].to = v[i].from + nrCifre/NR_THREADS - 1;
       v[i].data = malloc(n*sizeof(int));
       strcpy(v[i].data,data);
   }
   v[NR_THREADS-1].to = nrCifre;

   pthread_t tid[NR_THREADS];

   for(int i=0;i<NR_THREADS;i++)
   {
       pthread_create(&tid[i],NULL,thread,&v[i]);
   }

   for(int i=0;i<NR_THREADS;i++)
   {pthread_join(tid[i],&nr1);
   nr+=(int)(long)nr1;
   }


   printf("%d\n",nr);






    return 0;
}