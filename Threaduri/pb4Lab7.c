#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
void* thread(void* arg)
{
    for(;;)
    {
        srand(time(0));
        sleep(rand()%5);
        printf("Threadul: %d\n",(int)(long)arg);
    }
}
int main()
{
    int nr=9;
    pthread_t tids[9];
    int delete;

    for(int i=0;i<9;i++)
    pthread_create(&tids[i],NULL,thread,(void*)(long)(i+1));

    while(nr>0)
    {
        scanf("%d",&delete);
        pthread_cancel(tids[delete-1]);
        nr--;
    }

    for(int i=0;i<9;i++)
    pthread_join(tids[i],NULL);
    return 0;
}