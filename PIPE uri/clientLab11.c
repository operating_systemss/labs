#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#define FIFO1 "fifo1"
#define FIFO2 "fifo2"

int main()
{
    int fd1,fd2;

    int operand1;
    int operand2;
    char operatie;
    int rezultat;

    unlink(FIFO1);
    unlink(FIFO2);

    mkfifo(FIFO1,0600);
    mkfifo(FIFO2,0600);

    printf("Dati operanzii si operatia:\n");
    scanf("%d %c %d",&operand1,&operatie,&operand2);
    

    fd1=open(FIFO1,O_WRONLY);
    write(fd1,&operand1,sizeof(operand1));
    write(fd1,&operand2,sizeof(operand2));
    write(fd1,&operatie,sizeof(operatie));
    close(fd1);

    fd2=open(FIFO2,O_RDONLY);
    read(fd2,&rezultat,sizeof(rezultat));
    close(fd2);

    printf("%d\n",rezultat);

    unlink(FIFO1);
    unlink(FIFO2);


    return 0;
}