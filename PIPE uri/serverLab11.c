#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#define FIFO1 "fifo1"
#define FIFO2 "fifo2"


int main()
{
    int fd1,fd2;

    int operand1;
    int operand2;
    char operatie;
    int rezultat;

    fd1=open(FIFO1,O_RDONLY);
    read(fd1,&operand1,sizeof(operand1));
    read(fd1,&operand2,sizeof(operand2));
    read(fd1,&operatie,sizeof(operatie));
    close(fd1);

    printf("%d %c %d",operand1,operatie,operand2);

    switch (operatie)
    {
    case '+':
    rezultat=operand1+operand2;    
        break;
    case '-':
    rezultat=operand1-operand2;
    break;
    case '*':
    rezultat=operand1*operand2;
    break;
    case '/':
    rezultat=operand1/operand2;
    break;

    default:
    printf("Operatie invalida\n");
    rezultat=0;
        break;
    }

    fd2=open(FIFO2,O_WRONLY);
    write(fd2,&rezultat,sizeof(rezultat));
    close(fd2);

    return 0;
}