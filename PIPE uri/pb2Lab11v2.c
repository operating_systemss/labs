#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#define FIFO_NAME "l11_my_fifo"

int main()
{
    int fd1 = -1;
    int x=1;
    char c='a';

    unlink(FIFO_NAME);

    if(mkfifo(FIFO_NAME,0600) != 0)
    {
        perror("Nu se poate crea pipe-ul");
        return -1;
    }

    fd1=open(FIFO_NAME,O_RDWR);
    if(fd1 == -1)
    {
        perror("Nu se poate crea pipe-ul");
        return -1;
    }

    while(1)
    {
        printf("%d\n",x);
        write(fd1,&c,sizeof(c));
        x++;
    }

    return 0;
}