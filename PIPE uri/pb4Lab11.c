#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <semaphore.h>

 int main(void)
{
    int fd[2];
    char c = 0;
    int i;
   
    sem_t *sem1;//citire
    sem_t *sem2;//scriere

    sem1=sem_open("semafor1",O_CREAT,0644,1);
    sem2=sem_open("semafor2",O_CREAT,0644,1);



    pipe(fd);

    if(fork() != 0)
    {
        c='a';
        sem_wait(sem1);
        sem_wait(sem2);

        for(i=0;i<10;i++)
        {
            write(fd[1],&c,sizeof(c));
            sem_post(sem2);
    
            sem_wait(sem1);
            read(fd[0],&c,sizeof(c));
            printf("Parent: %c\n", c);
            c++;

        }
        close(fd[0]);//citire
        close(fd[1]);//scriere
        wait(NULL);
    }
    else
    {
        for(i=0;i<10;i++)
        {
            sem_wait(sem2);
            read(fd[0],&c,sizeof(c));

            printf("Child: %c\n",c);
            c++;
            write(fd[1],&c,sizeof(c));
            sem_post(sem1);
        }
        close(fd[0]);
        close(fd[1]);
    }


 return 0;
 }