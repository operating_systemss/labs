#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <string.h>

int main(void){
    int fd[2];
    char str[100] = "laborator";
    int lungimeString;
    lungimeString=strlen(str);
    
    if(pipe(fd) != 0){
        perror("Could not create pipe");
        return 1;
    }

    if(fork() != 0){
        close(fd[0]);
        for(int i = 0; i < lungimeString; i ++){
            write(fd[1], &str[i], 1);
            printf("Parent: wrote %c to pipe\n", str[i]);
        }
        close(fd[1]);
        wait(NULL);
    } else {
        close(fd[1]);
        for(int i = 0; i < lungimeString; i ++){
            read(fd[0], &str[i], 1);
            printf("Child: read %c from pipe\n", str[i]);
        }
        close(fd[0]);
    }
    return 0;
}