#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>


int main()
{
    int fd[2];
    int x = 1;
    char c = 'c';

    if(pipe(fd) != 0)
    {
        perror("Eroare la deschiderea pipe-urilor");
        return -1;
    }

        while(1)
        {
            printf("%d\n",x);
            write(fd[1],&c,sizeof(c));
            x++;
        }
    


    return 0;
}