#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
int insert(int* v1, int n1, int c1, int* v2, int n2, int pos)
{
    if (n1 + n2 > c1)
        return -1;

    if (pos<0 || pos>c1 - n2)
        return -1;

    int i = 0;
    while (i < n2)
    {
        for (int j = n1;j >= pos;j--) 
            v1[j] = v1[j - 1];

        v1[pos] = v2[i];
        pos++;
        i++;
        n1++;
    }

    return 0;

}
int main()
{   int n1, n2, c1, pos;
    int* v2 = NULL;
    printf("Dati capacitatea lui v1: ");
    if(scanf("%d", &c1)!=1)
    {
        printf("Nu a functionat citirea");
        return -1;
    }
    int* v1 = (int*)calloc(c1, sizeof(int));
    if (v1 == NULL) {
        printf("Alocarea nu a avut succes!");
        exit(-1);
    }
    printf("Dati numarul de elemente din v1:");
    if(scanf("%d", &n1)!=1)
    {
        printf("Nu a functionat citirea");
        return -1;
    }
    printf("Dati numarul de elemente din v2:");
    if(scanf("%d", &n2)!=1)
    {
        printf("Nu a functionat citirea");
        return -1;
    }
    v2 = (int*)calloc(n2, sizeof(int));
    printf("Dati elementele din v1");
    for (int i = 0;i < n1;i++)
        if(scanf("%d", &v1[i])!=1)
        {
            printf("Nu a functionat citirea");
            return -1;
        }

    printf("Dati elementele din v2");
    for (int i = 0;i < n2;i++)
        if(scanf("%d", &v2[i])!=1)
        {
            printf("Nu a functionat citirea");
            return -1;
        }

    printf("Dati pozitia de la care se incepe inserarea:");
    if(scanf("%d", &pos)!=1)
    {
        printf("Nu a functionat citirea");
        return -1;
    }
    

    if (insert(v1, n1, c1, v2, n2, pos) == -1)
    {
        printf("Inserarea nu s-a putut realiza");
        return -1;
    }
    else
    {
        printf("Inserarea s-a realizat");
        for (int i = 0;i < n1 + n2;i++)
            printf("%d ", v1[i]);
        return 0;
    }


    
}


