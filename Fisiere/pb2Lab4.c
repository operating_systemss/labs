#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc,char** argv)
{
    if(argc != 3)
    {
        printf("format: %s <srcFile destFile>\n",argv[0]);
        return 1;
    }

    int fd1 = -1;
    int fd2 = -1;

    fd1 = open(argv[1],O_RDONLY);
    fd2 = open(argv[2],O_WRONLY);

    if(fd1 == -1 || fd2 == -1)
    {
        printf("Nu s-a putut deschide unul dintre fisiere\n");
        return 1;
    }

    int fileSize = 0;
    fileSize = lseek(fd1,0,SEEK_END);

    lseek(fd1,-1,SEEK_END);

    int i = 0;
    char c;

    while(i<fileSize)
    {
        read(fd1,&c,sizeof(char));

        write(fd2,(void*)&c,sizeof(char));

        i++;
        lseek(fd1,-2,SEEK_CUR);
    }

    close(fd1);
    close(fd2);



    return 0;
}