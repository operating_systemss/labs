#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
int inversLinii(int fd1,int fd2)
{
    int size=lseek(fd1,0,SEEK_END);
    lseek(fd1,0,SEEK_SET);
    lseek(fd2,0,SEEK_SET);
    char* buf=(char*)malloc(size*sizeof(char));
    for(int i=0;i<size;i++)
    read(fd1,&buf[i],1);

    for(int i=size-1;i>=0;i--)
    {if(buf[i]=='\n')
    {int j=i+1;
    while(buf[j]!='\n' && j<size)
    {
        write(fd2,(void*)&buf[j],1);
        j++;
    }
    write(fd2,"\n",1);
    }
    else{
    if(i==0)
    {int j=i;
        while(j<size)
        {
            write(fd2,(void*)&buf[j],1);
            if(buf[j]=='\n')
            break;
            j++;
        }
    }
    }
    }

    return 0;

}

int main(int argc,char** argv)
{
    if(argc!=3)
    {
        printf("format: %s <srcFile srcDst>",argv[0]);
        return 1;
    }

    int fd1 = -1;
    int fd2 = -1;

    fd1=open(argv[1],O_RDONLY);
    fd2=open(argv[2],O_WRONLY);

    if(fd1 == -1 || fd2 == -1)
    {
        printf("Nu s-au putut deschide fisierele\n");
        return 1;
    }
    inversLinii(fd1,fd2);


    return 0;
}