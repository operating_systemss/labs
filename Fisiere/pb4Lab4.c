#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

int main(int argc,char** argv)
{if(argc<3)
{perror("Nu sunt suficienti parametri");
return -1;}
int sizeString;
int size2=0;
int poz;
char str[50]={};
sscanf(argv[1],"%d",&poz);
printf("Dati stringul de inserat:\n");
scanf("%s",str);
sizeString=strlen(str);
int fd=open(argv[2],O_RDWR);
if(fd==-1)
{perror("Fisierul nu a putut fi deschis");
return -2;}
size2=lseek(fd,0,SEEK_END)-poz;
char *buf=(char*)malloc(size2*sizeof(char));
lseek(fd,poz,SEEK_SET);
if(read(fd,buf,size2)==-1)
{perror("Citirea nu s a putut realiza");
return -3;}
lseek(fd,poz+sizeString,SEEK_SET);
if(write(fd,buf,size2)==-1){
perror("Nu s a putut face scrierea");
return -4;}
lseek(fd,poz,SEEK_SET);
if(write(fd,str,sizeString)==-1){
perror("Nu s a putut face scrierea");
return -4;}

}