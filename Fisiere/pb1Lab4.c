#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int get_line(int fd,int lineNr,char *line,int maxLength)
{
    int fileSize = 0;
    fileSize = lseek(fd,0,SEEK_END);
    lseek(fd,0,SEEK_SET);
    char c;
    int lineNumber = 1;
    int i = 0;
    int contor = 0;
    while(i<fileSize)
    {
        read(fd,&c,sizeof(char));

        if(c == '\n')
        {lineNumber++;
        if(lineNumber>lineNr)
        break;
        }

        if(lineNumber == lineNr)
        {
            line[contor]=c;
            contor++;
            if(contor > maxLength)
            {printf("Linie prea lunga\n");
            return -2;
            }
        }

    }
    if(lineNumber<lineNr)
    {
        printf("Nu exista aceasta linie\n");
        return -1;
    }

    line[contor]='\0';

    return 0;
}

int main(int argc,char** argv)
{
    if(argc!=3)
    {
        printf("format: %s <fileName lineNumber>\n",argv[0]);
        return 1;
    }

    int fd=-1;
    int lineNr = 0;
    int maxLength = 0;
    char *line = NULL;

    fd=open(argv[1],O_RDONLY);

    if(fd == -1)
    {
        perror("Eroare la deshciderea fisierului");
        return 1;
    }

    lineNr = atoi(argv[2]);

    printf("Dati maxLength pentru buffer:\n");
    scanf("%d",&maxLength);

    if(maxLength<=0)
    {
        printf("Dimensiune invalida\n");
        return 1;
    }

    line = malloc(maxLength*sizeof(char));

    printf("%d\n",get_line(fd,lineNr,line,maxLength));
    printf("%s\n",line);


    return 0;
}